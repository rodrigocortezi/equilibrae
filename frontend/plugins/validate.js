import Vue from 'vue'
import {
	ValidationProvider,
	ValidationObserver,
	setInteractionMode,
	extend,
	configure,
	localize
} from 'vee-validate'
import{ required, confirmed, email, alpha_spaces, min } from 'vee-validate/dist/rules'
import pt_BR from 'vee-validate/dist/locale/pt_BR.json'

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

setInteractionMode('custom', ({ errors }) => {
	if(errors.length) {
		return {
			on: ['input', 'change']
		}
	}

	return {
		on: ['change']
	}
})

extend('required', required)
extend('email', email)
extend('alpha_spaces', alpha_spaces)
extend('min', min)
extend('confirmed', confirmed)
extend('phone', value => {
	const raw = value.replace(/[^\d]/g, '')
	return raw.length === 11
})

localize('pt_BR', pt_BR)
localize({
	pt_BR: {
		names: {
			name: 'nome',
			phone: 'celular',
			message: 'mensagem',
			password: 'senha',
			current_password: 'senha atual',
			category: 'categoria'
		},
		messages: {
	    "alpha": "O campo {_field_} deve conter somente letras",
	    "alpha_dash": "O campo {_field_} deve conter letras, números e traços",
	    "alpha_num": "O campo {_field_} deve conter somente letras e números",
	    "alpha_spaces": "O campo {_field_} só pode conter letras e espaços",
	    "between": "O campo {_field_} deve estar entre {min} e {max}",
	    "confirmed": "O campo não confere",
	    "digits": "O campo {_field_} deve ser numérico e ter exatamente {length} dígitos",
	    "dimensions": "O campo {_field_} deve ter {width} pixels de largura por {height} pixels de altura",
	    "email": "Preencha um {_field_} válido",
	    "excluded": "O campo {_field_} deve ser um valor válido",
	    "ext": "O campo {_field_} deve ser um arquivo válido",
	    "image": "O campo {_field_} deve ser uma imagem",
	    "integer": "O campo {_field_} deve ser um número inteiro",
	    "is": "O valor inserido no campo {_field_} não é válido",
	    "oneOf": "O campo {_field_} deve ter um valor válido",
	    "length": "O tamanho do campo {_field_} deve ser {length}",
	    "max": "O campo {_field_} não deve ter mais que {length} caracteres",
	    "max_value": "O campo {_field_} precisa ser {max} ou menor",
	    "mimes": "O campo {_field_} deve ser um tipo de arquivo válido",
	    "min": "O campo {_field_} deve conter pelo menos {length} caracteres",
	    "min_value": "O campo {_field_} precisa ser {min} ou maior",
	    "numeric": "O campo {_field_} deve conter apenas números",
	    "regex": "O campo {_field_} possui um formato inválido",
	    "required": "obrigatório",
	    "required_if": "O campo {_field_} é obrigatório",
	    "size": "O campo {_field_} deve ser menor que {size}KB",
	    "phone": "Preencha um {_field_} válido",
	    "cpf": "Preencha um {_field_} válido",
	    "date": "Preencha uma {_field_} válida",
	    "cep": "Preencha um {_field_} válido",
	    "maxlength": "Selecione até {length} {_field_}"
		}
	}
})

configure({
  classes: {
    invalid: 'is-danger'
  }
})
