import Vue from 'vue'

Vue.filter('productCategory', value => {
	const categories = {
		torta_pao: 'Torta-pão',
		paes_veganos: 'Pães veganos',
		quiches: 'Quiches',
		pizzas: 'Pizzas',
		doces: 'Doces'
	}

	return categories[value]
})
