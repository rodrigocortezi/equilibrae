import status from 'http-status'

const handleUnauthorizedError = (error, { $axios, $auth }) => {
	const config = error.config
	const loginUrl = $auth.strategy.options.access_token_endpoint

	if(config.url === loginUrl || config._retry === true) {
		return Promise.reject(error)
	}

	return $auth.strategy.reAuthenticate().then(result => {
		config._retry = true
		delete config.headers
		return $axios.request(config)
	})
}

export default function({ app, $axios }) {
	$axios.onError(error => {
		const responseStatus = parseInt(error.response && error.response.status)
		if(responseStatus === status.UNAUTHORIZED) {
			return handleUnauthorizedError(error, app)
		}
	})
}
