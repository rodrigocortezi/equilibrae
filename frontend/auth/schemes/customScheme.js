import LocalScheme from '@nuxtjs/auth/lib/schemes/local'
import { encodeQuery } from '@nuxtjs/auth/lib/core/utilities'

export default class CustomScheme extends LocalScheme {
  mounted() {
    const token = this.$auth.syncToken(this.name)
    this._setToken(token)
    this.$auth.onError((error, { method }) => {
      if (method === 'mounted') {
        this.reAuthenticate().then(() => this.fetchUser())
      }
    })

    return this.$auth.fetchUserOnce()
  }

	async login({ email, password }) {
    const data = await this.$auth.request({
      method: 'post',
      url: this.options.access_token_endpoint,
      data: encodeQuery({
      	email,
      	password,
        grant_type: this.options.grant_type
      })
    })

    const { token, refreshToken } = this._extractTokens(data)
    if (token === undefined) {
      return
    }

    await this.fetchUser()
    this.$auth.redirect('admin', true)
	}

  async fetchUser() {
    // Token is required but not available
    if (this.options.tokenRequired && !this.$auth.getToken(this.name)) {
      return
    }

    // User endpoint is disabled.
    if (!this.options.userinfo_endpoint) {
      this.$auth.setUser({})
      return
    }

    // Try to fetch user and then set
    const user = await this.$auth.requestWith(this.name,
      { url: this.options.userinfo_endpoint }
    )
    this.$auth.setUser(user)
  }

  _extractTokens(responseData) {
    let token = responseData[this.options.token_key]
    let refreshToken = responseData[this.options.refresh_token_key]

    if (!token || !token.length) {
      return {}
    }

    if (this.options.tokenType) {
      token = this.options.tokenType + ' ' + token
    }

    this.$auth.setToken(this.name, token)

    this._setToken(token)

    if (refreshToken && refreshToken.length) {
      refreshToken = this.options.tokenType + ' ' + refreshToken
      this.$auth.setRefreshToken(this.name, refreshToken)
    }

    return { token, refreshToken }
  }

  reAuthenticate() {
    return this._refreshToken().then(result => {
      if(result) {
        return Promise.resolve()
      } else {
        this.$auth.reset()
        this.$auth.redirect('login', true)
        return Promise.reject()
      }
    })
  }

  async _refreshToken() {
    let refreshToken = this.$auth.getRefreshToken(this.name)
    if(refreshToken === null) {
      return false
    }

    let token = refreshToken.split(' ')[1]
    try {
      const data = await this.$auth.request({
        method: 'post',
        url: this.options.access_token_endpoint,
        data: encodeQuery({
          grant_type: this.options.refresh_token_key,
          refresh_token: token
        })
      })

      let { token: extractedToken } = this._extractTokens(data)
      if (extractedToken === undefined) {
        return false
      }

      return true
    } catch {
      return false
    }
  }
}
