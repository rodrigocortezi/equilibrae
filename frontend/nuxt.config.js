const isDev = process.env.NODE_ENV === 'development'
const host = process.env.APP_HOST

export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  generate: {
    exclude: [
      /^\/admin/
    ]
  },
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: 'Equilibraê',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Comida de verdade com muito sabor' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Equilibraê' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'twitter:site', name: 'twitter:site', content: '@equilibrae' },
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary_large_image'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://equilibrae.com.br/equilibrae-card.png'
      },
      {
        hid: 'og:image:secure_url',
        property: 'og:image:secure_url',
        content: 'https://equilibrae.com.br/equilibrae-card.png'
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: 'Transporte Machado'
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: 'https://equilibrae.com.br/equilibrae-card.png'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#FCCF62' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/global.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    '~/plugins/filters',
    '~/plugins/axios',
    '~/plugins/validate'
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/google-fonts',
    '@nuxtjs/svg'
  ],
  googleFonts: {
    families: {
      Inter: [300, 600, 800]
    }
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'nuxt-buefy',
    ['vue-scrollto/nuxt', { container: 'html' }]
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: isDev ? 'http://api:3000' : `http://api.${host}`,
    browserBaseURL: isDev ? 'http://localhost:3000' : `http://api.${host}`,
    https: !isDev
  },
  /*
  ** Auth module configuration
  ** See https://auth.nuxtjs.org/#getting-started
  */
  auth: {
    redirect: {
      admin: '/admin',
      login: '/admin/login',
      logout: '/admin'
    },
    cookie: {
      options: {
        secure: !isDev
      }
    },
    localStorage: false,
    watchLoggedIn: false,
    defaultStrategy: 'customStrategy',
    strategies: {
      customStrategy: {
        _scheme: '~/auth/schemes/customScheme',
        access_token_endpoint: '/oauth/token',
        token_key: 'access_token',
        refresh_token_key: 'refresh_token',
        grant_type: 'password',
        userinfo_endpoint: '/v1/me'
      }
    }
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    transpile: [
      '@nuxtjs/auth',
      'vee-validate/dist/rules'
    ]
  }
}
