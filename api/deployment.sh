#!/bin/bash

if db_version=$(bundle exec rake db:version 2>/dev/null)
then
	bundle exec rake db:migrate
else
	bundle exec rake db:setup
fi

exec "$@"