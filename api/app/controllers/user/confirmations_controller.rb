class User::ConfirmationsController < Devise::ConfirmationsController
	protected

		def after_confirmation_path_for(resource_name, resource)
			url = ENV['APP_HOST']
			suffix = '/admin/login?confirmation=true'
			if url.present?
				url = 'https://' + url
			else
				url = 'http://localhost:5000'
			end

			url += suffix
		end
end
