class User::RegistrationsController < Devise::RegistrationsController
  before_action :doorkeeper_authorize!, only: :update
  skip_before_action :authenticate_scope!, only: :update

	def update
		resource = current_resource_owner
		resource_updated = update_resource(resource, account_update_params)
		if resource_updated
			head :ok
		else
			respond_with resource
		end
	end
end
