class Api::V1::ProductsController < ApplicationController
  before_action :doorkeeper_authorize!, only: [:create, :update, :destroy]
  before_action :set_product, only: [:show, :update, :destroy]

  # GET /products
  def index
    @products = Product.all

    render json: @products
  end

  # GET /products/1
  def show
    render json: @product
  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      render json: @product, status: :created
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(
        :name,
        :description,
        :wheight,
        :ingredients,
        :category,
        photo: [
          :image
        ],
        nutrition: [
          :id,
          :wheight,
          :_destroy,
          facts: [
            :id,
            :name,
            :value,
            :_destroy
          ]
        ]
      ).tap do |params|
        unless params[:photo].nil?
          params[:photo_attributes] = params[:photo]
          params.delete(:photo)
        end

        unless params[:nutrition].nil?
          params[:nutrition_attributes] = params[:nutrition]
          params[:nutrition_attributes][:facts_attributes] = params[:nutrition][:facts]
          params.delete(:nutrition)
          params[:nutrition_attributes].delete(:facts)
        end
      end
    end
end
