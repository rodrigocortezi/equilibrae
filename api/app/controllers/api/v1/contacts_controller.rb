class Api::V1::ContactsController < ApplicationController
	def create
		contact_type = params[:type]
		if contact_type == 'partnerships'
			ContactMailer.with(contact_params).partnerships_email.deliver_later
		elsif contact_type == 'resellers'
			ContactMailer.with(contact_params).resellers_email.deliver_later
		else
			head :unprocessable_entity
		end
	end

	private

		def contact_params
			params.require(:contact).permit(:name, :email, :phone, :message)
		end
end
