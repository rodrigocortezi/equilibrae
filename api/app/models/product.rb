class Product < ApplicationRecord
	has_one :nutrition
	has_one :photo, as: :imageable, dependent: :destroy
	accepts_nested_attributes_for :photo, allow_destroy: true
	accepts_nested_attributes_for :nutrition, allow_destroy: true

	enum category: {
		torta_pao: 'Torta-pão',
		paes_veganos: 'Pães veganos',
		quiches: 'Quiches',
		pizzas: 'Pizzas',
		doces: 'Doces'
	}

	def as_json(options={})
		json = super(options).tap do |j|
			unless self.photo.nil?
				j.merge!({
					photo: {
						url: photo.image.url,
						filename: photo.image.original_filename
					}
				})
			end

			unless self.nutrition.nil?
				j.merge!({
					nutrition: nutrition.as_json
				})
			end
		end
	end
end
