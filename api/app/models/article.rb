class Article < ApplicationRecord
	has_one :photo, as: :imageable, dependent: :destroy
	accepts_nested_attributes_for :photo, allow_destroy: true

	def as_json(options={})
		json = super(options).tap do |j|
			unless self.photo.nil?
				j.merge!({
					photo: {
						url: photo.image.url,
						filename: photo.image.original_filename
					}
				})
			end
		end
	end
end