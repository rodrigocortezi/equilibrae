class Nutrition < ApplicationRecord
	belongs_to :product
	has_many :facts, class_name: :NutritionFact, dependent: :destroy
	accepts_nested_attributes_for :facts, allow_destroy: true

	def as_json(options={})
		json = super(options).tap do |j|
			unless self.facts.nil?
				j.merge!({
					facts: facts.as_json
				})
			end
		end
	end
end
