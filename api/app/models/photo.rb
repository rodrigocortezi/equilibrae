class Photo < ApplicationRecord
	include Shrine::Attachment(:image)
	belongs_to :imageable, polymorphic: true
end
