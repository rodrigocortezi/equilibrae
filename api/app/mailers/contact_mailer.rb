class ContactMailer < ApplicationMailer
	default from: 'contato-website@equilibrae.com.br'
	if Rails.env.production?
		default to: 'equilibraegastronomiasaudavel@gmail.com'
	else
		default to: 'dev@clarific.com.br'
	end

	def partnerships_email
		email = params[:email]
		@name = params[:name]
		@phone = params[:phone]
		@message = params[:message]
		mail(reply_to: email, subject: 'Contato via website: parcerias')
	end

	def resellers_email
		email = params[:email]
		@name = params[:name]
		@phone = params[:phone]
		@message = params[:message]
		mail(reply_to: email, subject: 'Contato via website: revendedores')
	end
end
