class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@equilibrae.com.br"
  layout 'mailer'
end
