class CreateNutritions < ActiveRecord::Migration[6.0]
  def change
    create_table :nutritions do |t|
      t.references :product, foreign_key: true, null: false
    	t.integer :servings, default: 1
    	t.string :wheight
    	t.json :facts

      t.timestamps
    end
  end
end
