class CreatePhotos < ActiveRecord::Migration[6.0]
  def change
    create_table :photos do |t|
    	t.references :imageable, null: false, polymorphic: true, index: false
    	t.text :image_data, null: false
    end
  end
end
