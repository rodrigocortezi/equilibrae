class CreateNutritionFacts < ActiveRecord::Migration[6.0]
  def change
    create_table :nutrition_facts do |t|
      t.references :nutrition, foreign_key: true, index: true, null: false
      t.string :name
      t.string :value
    end
    remove_column :nutritions, :facts, :json
  end
end
