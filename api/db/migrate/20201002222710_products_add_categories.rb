class ProductsAddCategories < ActiveRecord::Migration[6.0]
  def up
  	add_column :products, :category, :string
  	execute <<-SQL
  		CREATE TYPE product_categories AS ENUM (
  			'Torta-pão',
  			'Pães veganos',
  			'Quiches',
  			'Pizzas',
  			'Doces'
  		);
      ALTER TABLE products ALTER COLUMN category TYPE product_categories USING category::product_categories;
  	SQL
  end

  def down
  	remove_column :products, :category
  end
end
