Rails.application.routes.draw do
	use_doorkeeper do
		skip_controllers :authorizations, :applications,
			:authorized_applications
	end

	devise_for :users, skip: :all
	devise_scope :user do
		scope :auth, defaults: { format: :json } do
			# post   '/signup',       to: 'devise/registrations#create'
			put    '/account',      to: 'user/registrations#update'
			# delete '/account',      to: 'devise/registrations#destroy'
			# put    '/password',     to: 'devise/passwords#update'
			# post   '/password',     to: 'devise/passwords#create'
			# get    '/confirmation', to: 'user/confirmations#show'
		end
	end

	scope module: 'api' do
		namespace :v1 do
			get 'me', action: :show, controller: 'users'
		  resources :products
		  resources :articles
		  post '/contact/:type', to: 'contacts#create'
		end
	end
end
