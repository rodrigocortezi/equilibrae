require 'shrine'
require 'shrine/storage/file_system'

Shrine.storages = {
  store: Shrine::Storage::FileSystem.new('public', prefix: 'uploads')
}

Shrine.plugin :activerecord
Shrine.plugin :model, cache: false
if Rails.env.development?
	Shrine.plugin :url_options, store: { host: 'http://localhost:3000' }
else
	Shrine.plugin :url_options, store: { host: "https://api.#{ENV.fetch('APP_HOST') { 'equilibrae.com.br' }}" }
end
